/* 
 * DEVIL.NET: This is the portable high-level binding of the most popular image library, DEVIL.
 * Copyright (C) 2006 Steven Rodriguez
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
// created on 22/03/2006 at 22:50
using System;
using Tao.DevIl;
/*
FIXME: APPLY THIS
    
    ILenum Origin; // origin of the image    
    ILuint   NumNext; // number of images following
    ILuint   NumMips; // number of mipmaps
    ILuint   NumLayers; // number of layers
*/
namespace DevIlDotNet
{
	public class Image
	{
		//IMAGE ID
		private int imageID;

		//-----------------------------------------------------------------------
		//CONSTRUCTORS
		//-----------------------------------------------------------------------
		
		//FIXED: FIX PREVIEW.
		public Image(string FileName,DevIlDotNet.ImageExtension Extension)
		{
			Tao.DevIl.Il.ilGenImages(1,out imageID);
			Tao.DevIl.Il.ilBindImage(imageID);
			Tao.DevIl.Il.ilLoad(Convert.ToInt32(Extension),FileName);					
		}
		
		//FIXED: FIX PREVIEW.
		internal Image(int ID)
		{
			imageID = ID;		
		}	
		
		//-----------------------------------------------------------------------
		//OPERATORS
		//-----------------------------------------------------------------------
		
		public static bool operator==(Image x,Image y)
		{
			Tao.DevIl.Il.ilBindImage(x.imageID);
			return Tao.DevIl.Ilu.iluCompareImage(y.imageID);
		}
	
		public static bool operator!=(Image x,Image y)
		{
			Tao.DevIl.Il.ilBindImage(x.imageID);
			if (Tao.DevIl.Ilu.iluCompareImage(y.imageID) == true)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		
		public override int GetHashCode()
		{
			return imageID;			
		}
		
		public override bool Equals(object obj)
		{
			if(obj == null | obj.GetType() != GetType())
			{
				return false;
			}
			else
			{
				Image temp = (Image)obj;
				return this.imageID == temp.imageID;
			}
		}
		
		//-----------------------------------------------------------------------
		//METHODS
		//-----------------------------------------------------------------------					
		//--------------------------
		//GENERAL METHODS
		//--------------------------
		
		//FIXED: FIX PREVIEW.
		internal int ID()
		{
			return imageID;
		}
		
		//FIXED: FIX PREVIEW.
		public void Close()
		{
			Tao.DevIl.Il.ilDeleteImages(1,ref imageID);
		}
		
		//FIXED: FIX PREVIEW.
		public void Save(string FileName,DevIlDotNet.ImageExtension Extension)
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			Tao.DevIl.Il.ilSave(Convert.ToInt32(Extension),FileName);
		}	
		
		//--------------------------
		//IMAGE-IN GENERALS
		//--------------------------
		
		//FIXED: FIX PREVIEW.
		public void Mirror(DevIlDotNet.MirrorType Type)
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			switch(Type)
			{
				case DevIlDotNet.MirrorType.Horizontal:
					Tao.DevIl.Ilu.iluFlipImage();
					break;
				case DevIlDotNet.MirrorType.Vertical:
					Tao.DevIl.Ilu.iluMirror();
					break;
			}
		}
		
		//FIXED: FIX PREVIEW.
		public void Clear(float Red,float Green,float Blue,float Alpha)
		{		
			Tao.DevIl.Il.ilClearColour(Red,Green,Blue,Alpha);
			Tao.DevIl.Il.ilClearImage();
		}
		
		//FIXED: FIX PREVIEW.
		public void Overlay(DevIlDotNet.Image DestImage,DevIlDotNet.Vector3 Position)
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			Tao.DevIl.Il.ilOverlayImage(DestImage.ID(),Position.X,Position.Y,Position.Z);
		}
		
		//FIXED: FIX PREVIEW.
		public void Blit(DevIlDotNet.Image DestImage,DevIlDotNet.Vector3 DestPosition,DevIlDotNet.Vector3 SrcPosition,int SrcWidth,int SrcHeight,int SrcDepth)
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			Tao.DevIl.Il.ilBlit(DestImage.ID(),DestPosition.X,DestPosition.Y,DestPosition.Z,SrcPosition.X,SrcPosition.Y,SrcPosition.Z,SrcWidth,SrcHeight,SrcDepth);
		}
		
		//FIXED: FIX PREVIEW.
		public void Copy(DevIlDotNet.Image DestImage)
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			Tao.DevIl.Il.ilCopyImage(DestImage.ID());
		}	
		
		//FIXED: FIX PREVIEW.
		public DevIlDotNet.Palette ImagePalette
		{
			get
			{
				return new DevIlDotNet.Palette(this);
			}
		}
		
		//FIXED: FIX PREVIEW.
		public void Scale(int Height,int Width,int Depth,DevIlDotNet.ScalingType Type)
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			Tao.DevIl.Ilu.iluImageParameter(Tao.DevIl.Ilu.ILU_FILTER,Convert.ToInt32(Type));
			Tao.DevIl.Ilu.iluScale(Width,Height,Depth);
		}	
		
		//FIXED: FIX PREVIEW.
		public void Crop(int PreserveX,int PreserveY,int PreserveWidth,int PreserveHeight)
		{
			Tao.DevIl.Ilu.iluCrop(PreserveX,PreserveY,0,PreserveWidth,PreserveHeight,this.Depth);
		}
		
		//FIXED: FIX PREVIEW.
		public void Rotate2D(float Degrees)
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			Tao.DevIl.Ilu.iluRotate(Degrees);
		}
		
		//FIXED: FIX PREVIEW.
		public void Rotate3D(DevIlDotNet.Vector3 position,float Degrees)
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			Tao.DevIl.Ilu.iluRotate3D(position.X,position.Y,position.Z,Degrees);
		}
		
		//FIXED: FIX PREVIEW
		public byte[] GetData()
		{
			byte[] data = new byte[this.ImageSize];
			Tao.DevIl.Il.ilBindImage(imageID);
			
			unsafe
			{
				System.IntPtr bytearray = Tao.DevIl.Il.ilGetData();
				byte* byteptr = (byte*)bytearray;
				
				for(int i = 0; i <= data.Length - 1; i++)
				{
					data[i] = byteptr[i];
				}
			}
			
			return data;
		}
		
		//FIXED: FIX PREVIEW
		internal byte[] GetData(int Index,int Length)
		{
			byte[] data = new byte[Length];
			Tao.DevIl.Il.ilBindImage(imageID);
			
			unsafe
			{
				System.IntPtr bytearray = Tao.DevIl.Il.ilGetData();
				byte* byteptr = (byte*)bytearray;
				
				for(int i = 0; i <= Length - 1; i++)
				{
					data[i] = byteptr[Index + i];
				}
			}
			
			return data;
		}
		
		//FIXED: FIX PREVIEW
		public Color[,] GetPixels()
		{
			//DEVIL IMAGE ALGORITHM:
			//--------------------------------------------------------------------
			//N PIXEL DATA = (((WIDTH * (Py - 1)) + Px) -1) * FORMATTYPE
			//N PIXEL DATA (STARTING AT INDEX 0) = (((WIDTH * ((Py + 1) - 1)) + (Px + 1)) -1) * FORMATTYPE
			//____________________________________________________________
			//FORMATTYPE [RGB/BGR] = 3
			//FORMATTYPE [RGBA/BGRA] = 4
			//FORMATTYPE [LUMINANCE] = 1
			//FORMATTYPE [COLORINDEX] = 1
			//--------------------------------------------------------------------
			Color[,] Pixels = new Color[this.Width,this.Height];
			byte[] ImageData = this.GetData();
			
			switch(this.Format)
			{
					case DevIlDotNet.ImageFormat.Bgr:
					for(int i = 0; i <= this.Width - 1; i++)
					{
						for(int a = 0; a <= this.Height - 1; a++)
						{
							int algorithm = ((((this.Width * ((a + 1) - 1)) + (i + 1)) -1) * 3);
							Pixels[i,a] = new Color(255.0f,Convert.ToSingle(ImageData[algorithm + 2]),Convert.ToSingle(ImageData[algorithm + 1]),Convert.ToSingle(ImageData[algorithm + 0]));						
						}
					}
					break;
				case DevIlDotNet.ImageFormat.Bgra:
					for(int i = 0; i <= this.Width - 1; i++)
					{
						for(int a = 0; a <= this.Height - 1; a++)
						{
							int algorithm = ((((this.Width * ((a + 1) - 1)) + (i + 1)) -1) * 3);
							Pixels[i,a] = new Color(Convert.ToSingle(ImageData[algorithm + 3]),Convert.ToSingle(ImageData[algorithm + 2]),Convert.ToSingle(ImageData[algorithm + 1]),Convert.ToSingle(ImageData[algorithm + 0]));						
						}
					}
					break;
				case DevIlDotNet.ImageFormat.ColorIndex:
					for(int i = 0; i <= this.Width - 1; i++)
					{
						for(int a = 0; a <= this.Height - 1; a++)
						{
							int algorithm = ((((this.Width * ((a + 1) - 1)) + (i + 1)) -1) * 1);
							Pixels[i,a] = new Color(255.0f,Convert.ToSingle(ImageData[algorithm + 0]),Convert.ToSingle(ImageData[algorithm + 0]),Convert.ToSingle(ImageData[algorithm + 0]));						
						}
					}
					break;
				case DevIlDotNet.ImageFormat.Luminance:
					for(int i = 0; i <= this.Width - 1; i++)
					{
						for(int a = 0; a <= this.Height - 1; a++)
						{
							int algorithm = ((((this.Width * ((a + 1) - 1)) + (i + 1)) -1) * 1);
							Pixels[i,a] = new Color(255.0f,Convert.ToSingle(ImageData[algorithm + 0]),Convert.ToSingle(ImageData[algorithm + 0]),Convert.ToSingle(ImageData[algorithm + 0]));						
						}
					}
					break;
				case DevIlDotNet.ImageFormat.Rgb:
					for(int i = 0; i <= this.Width - 1; i++)
					{
						for(int a = 0; a <= this.Height - 1; a++)
						{
							int algorithm = ((((this.Width * ((a + 1) - 1)) + (i + 1)) -1) * 3);
							Pixels[i,a] = new Color(255.0f,Convert.ToSingle(ImageData[algorithm + 0]),Convert.ToSingle(ImageData[algorithm + 1]),Convert.ToSingle(ImageData[algorithm + 2]));						
						}
					}
					break;
					case DevIlDotNet.ImageFormat.Rgba:
					for(int i = 0; i <= this.Width - 1; i++)
					{
						for(int a = 0; a <= this.Height - 1; a++)
						{
								int algorithm = ((((this.Width * ((a + 1) - 1)) + (i + 1)) -1) * 4);
								Pixels[i,a] = new Color(Convert.ToSingle(ImageData[algorithm + 3]),Convert.ToSingle(ImageData[algorithm + 0]),Convert.ToSingle(ImageData[algorithm + 1]),Convert.ToSingle(ImageData[algorithm + 2]));						
						}
					}
					break;
			}
			
			return Pixels;
		}
		
		//FIXED: FIX PREVIEW
		public Color GetPixel(int XPosition,int YPosition)
		{
			//DEVIL IMAGE ALGORITHM:
			//--------------------------------------------------------------------
			//N PIXEL DATA = (((WIDTH * (Py - 1)) + Px) -1) * FORMATTYPE
			//N PIXEL DATA (STARTING AT INDEX 0) = (((WIDTH * ((Py + 1) - 1)) + (Px + 1)) -1) * FORMATTYPE
			//____________________________________________________________
			//FORMATTYPE [RGB/BGR] = 3
			//FORMATTYPE [RGBA/BGRA] = 4
			//FORMATTYPE [LUMINANCE] = 1
			//FORMATTYPE [COLORINDEX] = 1
			//--------------------------------------------------------------------
			Color Pixel = new Color(0.0f,0.0f,0.0f,0.0f);			
			int algorithm;
			byte[] PixelData;
			
			switch(this.Format)
			{
				case DevIlDotNet.ImageFormat.Bgr:
					algorithm = ((((this.Width * ((YPosition + 1) - 1)) + (XPosition + 1)) -1) * 3);
					PixelData = this.GetData(algorithm,3);
					Pixel = new Color(255.0f,Convert.ToSingle(PixelData[2]),Convert.ToSingle(PixelData[1]),Convert.ToSingle(PixelData[0]));			
					break;
				case DevIlDotNet.ImageFormat.Bgra:
					algorithm = ((((this.Width * ((YPosition + 1) - 1)) + (XPosition + 1)) -1) * 4);
					PixelData = this.GetData(algorithm,4);
					Pixel = new Color(Convert.ToSingle(PixelData[3]),Convert.ToSingle(PixelData[2]),Convert.ToSingle(PixelData[1]),Convert.ToSingle(PixelData[0]));						
					break;
				case DevIlDotNet.ImageFormat.ColorIndex:
					algorithm = ((((this.Width * ((YPosition + 1) - 1)) + (XPosition + 1)) -1) * 1);
					PixelData = this.GetData(algorithm,1);
					Pixel = new Color(255.0f,Convert.ToSingle(PixelData[0]),Convert.ToSingle(PixelData[0]),Convert.ToSingle(PixelData[0]));						
					break;
				case DevIlDotNet.ImageFormat.Luminance:
					algorithm = ((((this.Width * ((YPosition + 1) - 1)) + (XPosition + 1)) -1) * 1);
					PixelData = this.GetData(algorithm,1);
					Pixel = new Color(255.0f,Convert.ToSingle(PixelData[0]),Convert.ToSingle(PixelData[0]),Convert.ToSingle(PixelData[0]));						
					break;
				case DevIlDotNet.ImageFormat.Rgb:
					algorithm = ((((this.Width * ((YPosition + 1) - 1)) + (XPosition + 1)) -1) * 3);
					PixelData = this.GetData(algorithm,3);
					Pixel = new Color(255.0f,Convert.ToSingle(PixelData[0]),Convert.ToSingle(PixelData[1]),Convert.ToSingle(PixelData[2]));											
					break;
				case DevIlDotNet.ImageFormat.Rgba:
					algorithm = ((((this.Width * ((YPosition + 1) - 1)) + (XPosition + 1)) -1) * 4);
					PixelData = this.GetData(algorithm,4);
					Pixel = new Color(Convert.ToSingle(PixelData[3]),Convert.ToSingle(PixelData[0]),Convert.ToSingle(PixelData[1]),Convert.ToSingle(PixelData[2]));					
					break;
			}
			return Pixel;
		}
		//--------------------------
		//IMAGE-IN EFFECTS
		//--------------------------
		
		//FIXED: FIX PREVIEW.
		public void Alienify()
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			Tao.DevIl.Ilu.iluAlienify();
		}
		
		//BETWEEN 0.0f - 1.0f
		//FIXED: FIX PREVIEW.
		public void Noisify(float Tolerance)
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			Tao.DevIl.Ilu.iluNoisify(Tolerance);
		}
		
		//More tan 1, if 1 = ERROR
		//FIXED: FIX PREVIEW.
		public void Pixelize(int PixelSize)
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			Tao.DevIl.Ilu.iluPixelize(PixelSize);
		}
		
		//FIXED: FIX PREVIEW.
		public void Negative()
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			Tao.DevIl.Ilu.iluNegative();
		}	
		
		//FIXED: FIX PREVIEW.
		public void InvertAlpha()
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			Tao.DevIl.Ilu.iluInvertAlpha();
		}		
		
		//FIXED: FIX PREVIEW.
		public void SwapColors()
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			Tao.DevIl.Ilu.iluSwapColours();
		}
		
		//FIXED: FIX PREVIEW.
		public void Equalize()
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			Tao.DevIl.Ilu.iluEqualize();
		}
		
		//FIXED: FIX PREVIEW.
		public void Emboss()
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			Tao.DevIl.Ilu.iluEmboss();
		}
		
		//PARAM -1.0f - 1.0f
		//FIXED: FIX PREVIEW.
		public void SetSaturation(float Red,float Green,float Blue,float Saturation)
		{
			Tao.DevIl.Il.ilBindImage(imageID);			
			Tao.DevIl.Ilu.iluSaturate4f(Red,Green,Blue,Saturation);		
		}	
		
		//FIXED: FIX PREVIEW.
		public void SetColorLevel(float Red,float Green, float Blue)
		{
			Tao.DevIl.Il.ilBindImage(imageID);	
			Tao.DevIl.Ilu.iluScaleColours(Red,Green,Blue);
		}
		
		//VALUES FATCOR 0.0f - 2.5f
		//FIXED: FIX PREVIEW.
		public void SetSharpening(float Factor,int Iterations)
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			Tao.DevIl.Ilu.iluSharpen(Factor,Iterations);
		}			
		
		//0.0f - 1.0f
		//FIXED: FIX PREVIEW.
		public void SetGammaCorrection(float Factor)
		{
			Tao.DevIl.Il.ilBindImage(imageID);			
			Tao.DevIl.Ilu.iluGammaCorrect(Factor);
		}
		
		//VALUES: 
		//FIXED: FIX PREVIEW.
		public void SetContrast(float Factor)
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			Tao.DevIl.Ilu.iluContrast(Factor);
		}		
		
		//FIXED: FIX PREVIEW.
		public void SetBlur(int Iterations,DevIlDotNet.BlurType Type)
		{
			Tao.DevIl.Il.ilBindImage(imageID);
			switch (Type)
			{
				case DevIlDotNet.BlurType.Average:
					Tao.DevIl.Ilu.iluBlurAvg(Iterations);
					break;
				case DevIlDotNet.BlurType.Gaussian:
					Tao.DevIl.Ilu.iluBlurGaussian(Iterations);
					break;
			}
		}	
		
		//-----------------------------------------------------------------------
		//PROPERTIES
		//-----------------------------------------------------------------------	
				
		//FIXED: FIX PREVIEW.
		public int Width
		{
			get
			{
				Tao.DevIl.Il.ilBindImage(imageID);
				return Tao.DevIl.Il.ilGetInteger(Tao.DevIl.Il.IL_IMAGE_WIDTH);
			}
		}
		
		//FIXED: FIX PREVIEW.
		public int Height
		{
			get
			{
				Tao.DevIl.Il.ilBindImage(imageID);
				return Tao.DevIl.Il.ilGetInteger(Tao.DevIl.Il.IL_IMAGE_HEIGHT);
			}
		}
		
		//FIXED: FIX PREVIEW.
		public int BytesPerPixel
		{
			get
			{
				Tao.DevIl.Il.ilBindImage(imageID);
				return Tao.DevIl.Il.ilGetInteger(Tao.DevIl.Il.IL_IMAGE_BYTES_PER_PIXEL);
			}
		}
		
		//FIXED: FIX PREVIEW.
		public int BitsPerPixel
		{
			get
			{
				Tao.DevIl.Il.ilBindImage(imageID);
				return Tao.DevIl.Il.ilGetInteger(Tao.DevIl.Il.IL_IMAGE_BITS_PER_PIXEL);
			}
		}
		
		//FIXED: FIX PREVIEW.
		public DevIlDotNet.ImageType Type
		{
			get
			{
				Tao.DevIl.Il.ilBindImage(imageID);				
				return (DevIlDotNet.ImageType)Tao.DevIl.Il.ilGetInteger(Tao.DevIl.Il.IL_IMAGE_TYPE);
			}
			set
			{
				Tao.DevIl.Il.ilConvertImage(Convert.ToInt32(this.Format),Convert.ToInt32(value));
			}
		}
		
		//FIXED: FIX PREVIEW.
		public DevIlDotNet.ImageFormat Format
		{
			get
			{
				Tao.DevIl.Il.ilBindImage(imageID);				
				return (DevIlDotNet.ImageFormat)Tao.DevIl.Il.ilGetInteger(Tao.DevIl.Il.IL_IMAGE_FORMAT);
			}
			set
			{				
				Tao.DevIl.Il.ilConvertImage(Convert.ToInt32(value),Convert.ToInt32(this.Type));
			}
		}
		
		//FIXED: FIX PREVIEW.
		public int Depth
		{
			get
			{
				Tao.DevIl.Il.ilBindImage(imageID);
				return Tao.DevIl.Il.ilGetInteger(Tao.DevIl.Il.IL_IMAGE_DEPTH);
			}
		}
		
		//FIXED: FIX PREVIEW.
		public int Bpp
		{
			get
			{
				Tao.DevIl.Il.ilBindImage(imageID);
				return Tao.DevIl.Il.ilGetInteger(Tao.DevIl.Il.IL_IMAGE_BPP);
			}
		}
		
		//FIXED: FIX PREVIEW.
		public int Bpc
		{
			get
			{
				Tao.DevIl.Il.ilBindImage(imageID);
				return Tao.DevIl.Il.ilGetInteger(Tao.DevIl.Il.IL_IMAGE_BPC);
			}
		}		
		
		//FIXED: FIX PREVIEW.
		public int ImageSize
		{
			get
			{
				Tao.DevIl.Il.ilBindImage(imageID);
				return Tao.DevIl.Il.ilGetInteger(Tao.DevIl.Il.IL_IMAGE_SIZE_OF_DATA);
			}
		}
		
		public int Colors
		{
			get
			{
				Tao.DevIl.Il.ilBindImage(imageID);
				return Tao.DevIl.Ilu.iluColoursUsed();
			}
		}
	}
}