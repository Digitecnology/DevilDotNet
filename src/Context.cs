/* 
 * DEVIL.NET: This is the portable high-level binding of the most popular image library, DEVIL.
 * Copyright (C) 2006 Steven Rodriguez
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
// created on 22/03/2006 at 22:50
using System;
using Tao.DevIl;

namespace DevIlDotNet
{
	public sealed class Context
	{
		
		//-----------------------------------------------------------------------
		//CONSTRUCTORS
		//-----------------------------------------------------------------------
		
		static Context()
		{
			
		}
		
		//-----------------------------------------------------------------------
		//METHODS
		//-----------------------------------------------------------------------
		
		//FIXED: FIX PREVIEW.
		public static void Initialize()
		{
			Tao.DevIl.Il.ilInit();
			Tao.DevIl.Ilu.iluInit();
		}
		
		//FIXME: IS THE AN ILU OR ILUT SHUTDOWN
		public static void Close()
		{
			Tao.DevIl.Il.ilShutDown();			
		}
		
		public static void SetMemory(MemoryMode Mode)
		{
			Tao.DevIl.Il.ilHint(Tao.DevIl.Il.IL_MEM_SPEED_HINT,Convert.ToInt32(Mode));	
		}
		
		public static void SetCompression(bool Value)
		{
			if(Value == true)
			{
				Tao.DevIl.Il.ilHint(Tao.DevIl.Il.IL_COMPRESSION_HINT,Tao.DevIl.Il.IL_USE_COMPRESSION);
			}
			else
			{
				Tao.DevIl.Il.ilHint(Tao.DevIl.Il.IL_COMPRESSION_HINT,Tao.DevIl.Il.IL_NO_COMPRESSION);
			}
		}	
		
		//-----------------------------------------------------------------------
		//PROPERTIES
		//-----------------------------------------------------------------------
		
		public static bool Overwriting
		{
			get
			{
				return Tao.DevIl.Il.ilIsEnabled(Tao.DevIl.Il.IL_FILE_OVERWRITE);
			}
			set
			{
				if(value == true)
				{
					Tao.DevIl.Il.ilEnable(Tao.DevIl.Il.IL_FILE_OVERWRITE);
				}
				else
				{
					Tao.DevIl.Il.ilDisable(Tao.DevIl.Il.IL_FILE_OVERWRITE);
				}
			}
		}
		
		//FIXED: FIX PREVIEW.
		public static string DevILVendor
		{
			get
			{			
				return Tao.DevIl.Il.ilGetString(Tao.DevIl.Il.IL_VENDOR);
			}
		}
		
		//FIXED: FIX PREVIEW.
		public static string DevILVersion
		{
			get
			{			
				return Tao.DevIl.Il.ilGetString(Tao.DevIl.Il.IL_VERSION);
			}
		}		
		
		//FIXED: FIX PREVIEW.
		public static string DevILUtilityVersion
		{
			get
			{					
				return Tao.DevIl.Ilu.iluGetString(Tao.DevIl.Ilu.ILU_VERSION);
			}
		}	
	}
}