/* 
 * DEVIL.NET: This is the portable high-level binding of the most popular image library, DEVIL.
 * Copyright (C) 2006 Steven Rodriguez
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
// created on 02/04/2006 at 4:54
using System;
using Tao.DevIl;

namespace DevIlDotNet
{
	public class Palette
	{
		int PalleteImageID;
		
		//-----------------------------------------------------------------------
		//CONSTRUCTORS
		//-----------------------------------------------------------------------
		
		//FIXED: FIX PREVIEW.
		public Palette(string FileName)
		{
			Tao.DevIl.Il.ilLoadPal(FileName);
		}
		
		//FIXED: FIX PREVIEW.
		internal Palette(DevIlDotNet.Image SrcImage)
		{
			PalleteImageID = SrcImage.ID();
		}	
		
		//-----------------------------------------------------------------------
		//METHODS
		//-----------------------------------------------------------------------
		
		//FIXED: FIX PREVIEW.
		public void Save(string FileName)
		{
			Tao.DevIl.Il.ilSavePal(FileName);
		}		
		
		public void Convert()
		{
		}
		
		//-----------------------------------------------------------------------
		//PROPERTIES
		//-----------------------------------------------------------------------
		
		//FIXED: FIX PREVIEW.
		public int Size
		{
			get
			{
				Tao.DevIl.Il.ilBindImage(PalleteImageID);
				return Tao.DevIl.Il.ilGetInteger(Tao.DevIl.Il.IL_PALETTE_SIZE);
			}
		}
		
		//FIXME: FIX THIS. IF USE ONLY CONVERT (NO SYSTEM) IT FAILS IN COMPILE.
		public DevIlDotNet.PaletteType PType
		{
			get
			{
				Tao.DevIl.Il.ilBindImage(PalleteImageID);
				return (DevIlDotNet.PaletteType)Tao.DevIl.Il.ilGetInteger(Tao.DevIl.Il.IL_PALETTE_TYPE);
			}
			set
			{
				Tao.DevIl.Il.ilBindImage(PalleteImageID);
				Tao.DevIl.Il.ilConvertPal(System.Convert.ToInt32(value));
			}
		}
		
		//FIXED: FIX PREVIEW.
		public int Bpp
		{
			get
			{
				Tao.DevIl.Il.ilBindImage(PalleteImageID);
				return Tao.DevIl.Il.ilGetInteger(Tao.DevIl.Il.IL_PALETTE_BPP);
			}
		}	
		
		//FIXED: FIX PREVIEW.
		public DevIlDotNet.Image GetImage()
		{
			return new DevIlDotNet.Image(PalleteImageID);
		}
		
		//FIXED: FIX PREVIEW.
		public int NumberOfColors
		{
			get
			{
				Tao.DevIl.Il.ilBindImage(PalleteImageID);
				return Tao.DevIl.Il.ilGetInteger(Tao.DevIl.Il.IL_PALETTE_NUM_COLS);
			}
		}
	}
}