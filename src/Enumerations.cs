/* 
 * DEVIL.NET: This is the portable high-level binding of the most popular image library, DEVIL.
 * Copyright (C) 2006 Steven Rodriguez
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
// created on 22/03/2006 at 22:49
using System;
using Tao.DevIl;

namespace DevIlDotNet
{
	public enum MemoryMode
	{
		Fast = Tao.DevIl.Il.IL_FASTEST,
		Less = Tao.DevIl.Il.IL_LESS_MEM
	};
	
	public enum ImageExtension
	{
		Bmp = Tao.DevIl.Il.IL_BMP,
		CHeader = Tao.DevIl.Il.IL_CHEAD,
		Cut = Tao.DevIl.Il.IL_CUT,
		Dcx = Tao.DevIl.Il.IL_DCX,
		Dds = Tao.DevIl.Il.IL_DDS,
		Doom = Tao.DevIl.Il.IL_DOOM,
		DoomFlat = Tao.DevIl.Il.IL_DOOM_FLAT,
		Gif = Tao.DevIl.Il.IL_GIF,
		Ico = Tao.DevIl.Il.IL_ICO,
		Jng = Tao.DevIl.Il.IL_JNG,
		Jpg = Tao.DevIl.Il.IL_JPG,
		Lbm = Tao.DevIl.Il.IL_LBM,
		Lif = Tao.DevIl.Il.IL_LIF,
		Mdl = Tao.DevIl.Il.IL_MDL,
		Mng = Tao.DevIl.Il.IL_MNG,
		//Oil = 0,
		Pcd = Tao.DevIl.Il.IL_PCD,
		Pcx = Tao.DevIl.Il.IL_PCX,
		Pic = Tao.DevIl.Il.IL_PIC,
		Png = Tao.DevIl.Il.IL_PNG,
		Pnm = Tao.DevIl.Il.IL_PNM,
		Psd = Tao.DevIl.Il.IL_PSD,
		Raw = Tao.DevIl.Il.IL_RAW,
		Sgi = Tao.DevIl.Il.IL_SGI,
		Tga = Tao.DevIl.Il.IL_TGA,
		Tif = Tao.DevIl.Il.IL_TIF,
		Wal = Tao.DevIl.Il.IL_WAL,
		Unknown = Tao.DevIl.Il.IL_TYPE_UNKNOWN
	};	
	
	public enum ScalingType
	{
		Nearest = Tao.DevIl.Ilu.ILU_NEAREST,
		Linear = Tao.DevIl.Ilu.ILU_LINEAR,
		Bilinear = Tao.DevIl.Ilu.ILU_BILINEAR,
		Box = Tao.DevIl.Ilu.ILU_SCALE_BOX,
		Triangle = Tao.DevIl.Ilu.ILU_SCALE_TRIANGLE,
		Bell = Tao.DevIl.Ilu.ILU_SCALE_BELL,
		BSpline = Tao.DevIl.Ilu.ILU_SCALE_BSPLINE,
		Lanczos = Tao.DevIl.Ilu.ILU_SCALE_LANCZOS3,
		Mitchell = Tao.DevIl.Ilu.ILU_SCALE_MITCHELL
	};
	
	public enum ErrorType
	{
		NoError = Tao.DevIl.Il.IL_NO_ERROR,
		InvalidEnum = Tao.DevIl.Il.IL_INVALID_ENUM,
		OutOfMemory = Tao.DevIl.Il.IL_OUT_OF_MEMORY,
		FormatNotSupported = Tao.DevIl.Il.IL_FORMAT_NOT_SUPPORTED,
		InternalError = Tao.DevIl.Il.IL_INTERNAL_ERROR,
		InvalidValue = Tao.DevIl.Il.IL_INVALID_VALUE,
		IllegalOperation = Tao.DevIl.Il.IL_ILLEGAL_OPERATION,
		IllegalFileValue = Tao.DevIl.Il.IL_ILLEGAL_FILE_VALUE,
		InvalidFileHeader = Tao.DevIl.Il.IL_INVALID_FILE_HEADER,
		InvalidParam = Tao.DevIl.Il.IL_INVALID_PARAM,
		CouldNotOpenFile = Tao.DevIl.Il.IL_COULD_NOT_OPEN_FILE,
		InvalidExtension = Tao.DevIl.Il.IL_INVALID_EXTENSION,
		FileAlreadyExists = Tao.DevIl.Il.IL_FILE_ALREADY_EXISTS,
		OutFormatSame = Tao.DevIl.Il.IL_OUT_FORMAT_SAME,
		StackOverflow = Tao.DevIl.Il.IL_STACK_OVERFLOW,
		StackUnderflow = Tao.DevIl.Il.IL_STACK_UNDERFLOW,
		InvalidConversion = Tao.DevIl.Il.IL_INVALID_CONVERSION,
		GifError = Tao.DevIl.Il.IL_LIB_GIF_ERROR,
		JpegError = Tao.DevIl.Il.IL_LIB_JPEG_ERROR,
		PngError = Tao.DevIl.Il.IL_LIB_PNG_ERROR,
		TiffError = Tao.DevIl.Il.IL_LIB_TIFF_ERROR,
		MngError = Tao.DevIl.Il.IL_LIB_MNG_ERROR,
		UnknownError = Tao.DevIl.Il.IL_UNKNOWN_ERROR
	};
	
	public enum MirrorType
	{
		Horizontal = 0,
		Vertical = 1
	};
	
	public enum BlurType
	{
		Average = 0,
		Gaussian = 1
	};	
	
	public enum PaletteType
	{
		Rgb24 = Tao.DevIl.Il.IL_PAL_RGB24,
		Rbg32 = Tao.DevIl.Il.IL_PAL_RGB32,
		Rgba32 = Tao.DevIl.Il.IL_PAL_RGBA32,
		Bgr24 = Tao.DevIl.Il.IL_PAL_BGR24,
		Bgr32 = Tao.DevIl.Il.IL_PAL_BGR32,
		Bgra32 = Tao.DevIl.Il.IL_PAL_BGRA32
	};
	
	public enum ImageFormat
	{
		Rgb = Tao.DevIl.Il.IL_RGB,
		Rgba = Tao.DevIl.Il.IL_RGBA,
		Bgr = Tao.DevIl.Il.IL_BGR,
		Bgra = Tao.DevIl.Il.IL_BGRA,
		Luminance = Tao.DevIl.Il.IL_LUMINANCE,
		ColorIndex = Tao.DevIl.Il.IL_COLOUR_INDEX
	};
	
	public enum ImageType
	{
		Byte = Tao.DevIl.Il.IL_BYTE,
		UByte = Tao.DevIl.Il.IL_UNSIGNED_BYTE,
		Short = Tao.DevIl.Il.IL_SHORT,
		UShort = Tao.DevIl.Il.IL_UNSIGNED_SHORT,
		Int = Tao.DevIl.Il.IL_INT,
		UInt = Tao.DevIl.Il.IL_UNSIGNED_INT,
		Float = Tao.DevIl.Il.IL_FLOAT,
		Double = Tao.DevIl.Il.IL_DOUBLE
	};
}
