/* 
 * DEVIL.NET: This is the portable high-level binding of the most popular image library, DEVIL.
 * Copyright (C) 2006 Steven Rodriguez
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

//---------------------------------------
// GENERAL ASSEMBLY INFORMATION
//---------------------------------------

[assembly: AssemblyTitle("DevIlDotNet")]
[assembly: AssemblyDescription("This is the portable high-level binding of the most popular image library, DEVIL.")]
[assembly: AssemblyCopyright("Copyright 2006 Steven Rodriguez. All rights reserved.")]
[assembly: AssemblyTrademark("http://devildotnet.sourceforge.net")]
[assembly: AssemblyDefaultAlias("DevIlDotNet")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("The DEVIL.NET Project")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyVersion("0.1")]
[assembly: AssemblyCulture("")]		
[assembly: NeutralResourcesLanguageAttribute("en-US")]
[assembly: AssemblyFileVersion("0.1")]
[assembly: AssemblyInformationalVersion("0.1")]
